linux-misc
==========

Miscellaneous shell and python scripts, tools, and utilities


Overview
========

These are files laid out like a Linux root filesystem, containing (arguably) useful scripts and configuration files.

Highlights include:

  * [A “perf” data file to CSV converter](usr/local/bin/perf2csv)
  * [My “Environment Modules” configuration files](usr/local/Modules/3.2.10/modulefiles/)
  * [A DiceWare dictionary generator](usr/local/bin/diceware-dict.sh)
  * [(De-)initialization scripts for CUDA, Intel VTune, HyperThreading support, “perf”, and msr.ko](usr/local/sbin/)
  * [Python VTK examples](usr/local/share/vtk/Examples/Python)
  * [No-IP network update script](etc/network/if-up.d/noip-update) and [corresponding logrotate config](etc/logrotate.d/noip-update)
